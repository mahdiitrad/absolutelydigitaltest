<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Storage;
class MainController extends Controller
{
  
	public function appointmentConflictCheck(Request $request)
    {
		$json = Storage::disk('local')->get('data.json');
		$data = json_decode($json, true);
		$conflict = [];
		$j = 0;
		
		for($x=0;$x<count($data);$x++)
		{
			for($y=0;$y<count($data);$y++)
			{
				if($data[$x]['start_date'] == $data[$y]['start_date'] && $data[$x]['id'] != $data[$y]['id'] || 
				   ($data[$x]['start_date'] > $data[$y]['start_date'] &&  $data[$x]['start_date'] < $data[$y]['end_date'] && $data[$x]['id'] != $data[$y]['id']))
				{
					$conflict[$j]['firstConflict'] = $data[$x];
					$conflict[$j]['secondConflict'] = $data[$y];
					$j++;
				}
			}
		}
		return $conflict ;
	}

}
